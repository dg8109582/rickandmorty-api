import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RymService {

  public URL = 'https://rickandmortyapi.com/api';

  constructor(private _http : HttpClient) { }

  getPersonajes(){
    const url = `${this.URL}/character`;
    return this._http.get(url);
  }
}

import { Component, OnInit } from '@angular/core';
import { RymService } from 'src/app/services/RickandMorty/rym.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public personajes : any [] = [];

  constructor(private _Rym : RymService) { }

  ngOnInit(): void {
    this.getPersonajes();
  }

  getPersonajes() {

    this.personajes = [];

    this._Rym.getPersonajes()
    .subscribe((data:any) => {
      console.log(data);

      this.personajes = data.results;
      

      console.log('Personajes SETEADOS',this.personajes);
      
    });

  }

  

}
